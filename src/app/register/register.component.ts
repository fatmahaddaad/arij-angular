import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { NotifierService } from 'angular-notifier';

import { ApplicationService } from '../services/application.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  private readonly notifier: NotifierService;
  email: string;
  username: string;
  password: string;
  password_confirm: string
  constructor(private applicationService: ApplicationService,
    notifierService: NotifierService,
    private router: Router,) { 
      this.notifier = notifierService;
    }

  ngOnInit() {
  }

  onRegister(){
    const user = {
      username : this.username,
      email : this.email,
      password : this.password
    }
    
    if (this.password != this.password_confirm) {
      this.notifier.notify( 'error', 'passwords don\'t match' );
    }
    if (this.username == undefined || this.username == null) {
      this.notifier.notify( 'error', 'username required' );
    }
    if (this.email == undefined || this.email == null) {
      this.notifier.notify( 'error', 'email required' );
    }
    if (this.password == undefined || this.password == null) {
      this.notifier.notify( 'error', 'password required' );
    }
    if (user.email != undefined && user.password != undefined && user.username != undefined) {
      this.applicationService.createUser(user).subscribe(res =>
      {
        this.notifier.notify( 'success', 'User account created successfully' );
        this.router.navigate([`/login/`]);
      }, (err) => {
        this.notifier.notify( 'error', 'An error occurred while creating a new user' );
      });
    }
  }
}
