import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { NotifierService } from 'angular-notifier';

import { ApplicationService } from '../services/application.service';

@Component({
  selector: 'app-supplier-request-product',
  templateUrl: './supplier-request-product.component.html',
  styleUrls: ['./supplier-request-product.component.css']
})
export class SupplierRequestProductComponent implements OnInit {

  label
  description
  type
  sizes
  colors
  price
  delivery_type
  delivery_time
  delivery_price
  stock
  categories
  public imagePath;
  imgURL: any;
  fileToUpload: File = null;
  public message: string;

  private readonly notifier: NotifierService;
  constructor(private applicationService: ApplicationService,
    notifierService: NotifierService,
    private router: Router,) { this.notifier = notifierService; }

  ngOnInit() {
    this.applicationService.categoriesPublicShow().subscribe(file => {
      this.categories = file.json();
      console.log(this.categories);
    })
  }

  handleFileInput(files) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    this.fileToUpload = files[0]; 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }
  cancel_upload() {
    this.imgURL = "";
  }

  onRequest(){
    const product = {
      label : this.label,
      type : this.type,
      sizes : this.sizes,
      colors : this.colors,
      price : this.price,
      delivery_type : this.delivery_type,
      delivery_time : this.delivery_time,
      delivery_price : this.delivery_price,
      description : this.description,
      stock : this.stock,
    }
    if(!product.sizes) {product.sizes = '';}
    if(!product.type) {product.type = '';}
    if(!product.description) {product.description = '';}
    if(!product.colors) {product.colors = '';}
    if(!product.delivery_type) {product.delivery_type = '';}
    if(!product.delivery_time) {product.delivery_time = '';}
    if(!product.delivery_price) {product.delivery_price = '';}
    if(!product.stock) {product.stock = '';}
    if (this.label == undefined || this.label == null) {
      this.notifier.notify( 'error', 'label required' );
    }
    if (this.price == undefined || this.price == null) {
      this.notifier.notify( 'error', 'price required' );
    }
    if (product.label != undefined && product.price != undefined) {
      this.applicationService.SupplierProductRequest(this.fileToUpload, product).subscribe(res =>
      {
        this.notifier.notify( 'success', 'Product created successfully' );
        this.router.navigate([`/`]);
      }, (err) => {
        this.notifier.notify( 'error', 'An error occurred while creating a new product' );
      });
    }
  }
}
