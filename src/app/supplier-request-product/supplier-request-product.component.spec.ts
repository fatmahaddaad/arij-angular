import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierRequestProductComponent } from './supplier-request-product.component';

describe('SupplierRequestProductComponent', () => {
  let component: SupplierRequestProductComponent;
  let fixture: ComponentFixture<SupplierRequestProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierRequestProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierRequestProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
