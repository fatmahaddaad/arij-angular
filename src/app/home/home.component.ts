import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators, FormControl  } from '@angular/forms';
import { NotifierService } from 'angular-notifier';

import { ApplicationService } from '../services/application.service';
import { CartService } from '../cart/service/cart.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  data;
  categories;
  creation_date;
  newProducts : any[] = []
  newList : any[] = []
  product;
  quantite=1;
  private readonly notifier: NotifierService;
  loadAPI: Promise<any>;
  constructor(private applicationService: ApplicationService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    notifierService: NotifierService,
    public cartservice:CartService,) { 
    this.loadAPI = new Promise((resolve) => {
      applicationService.loadScript("../../assets/user/js/main.js");
      resolve(true);
    })
  }

  ngOnInit() {
    this.applicationService.productsPublicShow().subscribe(file => {
      this.data = file.json();
      this.data.forEach(item => {
        this.creation_date = moment(item.date_creation);
        if (moment().diff(this.creation_date, 'days') < 30) {
          this.newProducts.push(item);
        }
      });
      console.log(this.newProducts);
    }, (err) => {}, 
    () => { 
      this.applicationService.categoriesPublicShow().subscribe(file => {
        this.categories = file.json();
        console.log(this.categories);
        this.newList = [this.categories[2], this.categories[1]]
        
      }, (err) => {}, 
      () => { 
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "./assets/user/js/sly.min.js";
        document.body.append(s)
        var c = document.createElement("script");
        c.type = "text/javascript";
        c.src = "./assets/user/js/main.js";
        document.body.append(c)
      })
    });
    // this.applicationService.categoriesPublicShow().subscribe(file => {
    //   this.categories = file.json();
    //   console.log(this.categories);
    //   let newList = [this.categories[0], this.categories[1]]
    //   console.log(newList);
      
    // })
  }

  AddToCart(item){
    this.product = item;
    this.product.size = item.sizes[0];
    this.product.color = item.colors[0];
    console.log(item);
   let cart={'id':null, 'product':item, 'quantite':this.quantite, "color":item.colors[0], "size":item.sizes[0]};
    this.cartservice.setNbrArticleInCart(cart);
  }
}
