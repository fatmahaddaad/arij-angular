import { Component, OnInit } from '@angular/core';
import { CartService } from './cart/service/cart.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'arij';
  constructor(public cartserv:CartService){}

  ngOnInit(){
    if(this.cartserv.verifLocalStorageCart()){
      let storage=JSON.parse(localStorage.getItem('carts'));
      if(storage.length > 0){
        this.cartserv.nbrArticleInCart = storage.length
      } else
        this.cartserv.nbrArticleInCart = 0
    } else
      this.cartserv.nbrArticleInCart = 0
  }
}
