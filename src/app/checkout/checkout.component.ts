import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApplicationService } from '../services/application.service';
import { CartService } from '../cart/service/cart.service';
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  rowscarts;
  totalCart = 0;
  constructor(public cartserv: CartService, 
    private router: Router,
    private applicationService:ApplicationService) { 
      if (cartserv.verifLocalStorageCart()) { 
        let storage = JSON.parse(localStorage.getItem('carts'))
        if (storage.length > 0) {
          this.rowscarts = storage;
          cartserv.carts = this.rowscarts
        }
        else
          this.rowscarts = cartserv.carts;
      }else{
        this.rowscarts =[]
        cartserv.carts = this.rowscarts
      }
    }

  ngOnInit() {
    console.log(this.rowscarts);
    this.totalCart = this.rowscarts.map(el => el.product.price * el.quantite).reduce((acc, el) => acc + el, 0);
    console.log(this.totalCart);
  }

}
