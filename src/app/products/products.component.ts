import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators, FormControl  } from '@angular/forms';
import { NotifierService } from 'angular-notifier';

import { ApplicationService } from '../services/application.service';
import { CartService } from '../cart/service/cart.service';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  data;
  product;
  quantite=1;
  private readonly notifier: NotifierService;
  constructor(private applicationService: ApplicationService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    notifierService: NotifierService,
    public cartservice:CartService,) { this.notifier = notifierService;}


  ngOnInit() {
    this.applicationService.productsPublicShow().subscribe(file => {
      this.data = file.json();
      console.log(this.data);
    });
  }

  AddToCart(item){
    this.product = item;
    this.product.size = item.sizes[0];
    this.product.color = item.colors[0];
    console.log(item);
   let cart={'id':null, 'product':item, 'quantite':this.quantite, "color":item.colors[0], "size":item.sizes[0]};
    this.cartservice.setNbrArticleInCart(cart);
  }
}
