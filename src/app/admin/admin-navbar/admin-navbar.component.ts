import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators, FormControl  } from '@angular/forms';

import { ApplicationService } from '../../services/application.service';
@Component({
  selector: 'app-admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.css']
})
export class AdminNavbarComponent implements OnInit {

  currentUserId
  data

  private readonly notifier: NotifierService;
  constructor(private applicationService: ApplicationService,
    private route: ActivatedRoute,
    private router: Router,
    notifierService: NotifierService) {this.notifier = notifierService; }


  ngOnInit() {
    let promise = new Promise((resolve, reject) => {
      this.applicationService.getCurrentUser()
        .toPromise()
        .then(
          res => { // Success
          this.currentUserId = res.json().isLogged.id;
          resolve();
          this.applicationService.getAdmiUserProfile(this.currentUserId).subscribe(file => {
            this.data = file.json()
            console.log(this.data);
          });
          },
          msg => { // Error
          reject(msg);
          }
        );
    });
    return promise;
  }
  onLogout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
    this.router.navigate([`/login`]);
  }
}
