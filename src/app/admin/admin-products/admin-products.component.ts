import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators, FormControl  } from '@angular/forms';
import { NotifierService } from 'angular-notifier';

import { ApplicationService } from '../../services/application.service';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit {
  data;
  private readonly notifier: NotifierService;
  constructor(private applicationService: ApplicationService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    notifierService: NotifierService,
    public auth: AuthService) { this.notifier = notifierService;}


  ngOnInit() {
    this.applicationService.getProducts().subscribe(file => {
      this.data = file.json();
      console.log(this.data);
    });
  }

  removeFromShop(id) {
    const product ={
      is_active : false
    }
    this.applicationService.removeProductFromShop(product, id).subscribe(res => {
      this.notifier.notify("success", "Product removed from shop")
      this.ngOnInit();
    }, (err) => {
      this.notifier.notify("error", err.json().message)
    })
  }
  addToShop(id) {
    const product ={
      is_active : true
    }
    this.applicationService.addProductToShop(product, id).subscribe(res => {
      this.notifier.notify("success", "Product added to shop")
      this.ngOnInit();
    }, (err) => {
      console.log(err.json());
      
      this.notifier.notify("error", err.json().message)
    })
  }
}
