import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators, FormControl  } from '@angular/forms';
import { NotifierService } from 'angular-notifier';

import { ApplicationService } from '../../services/application.service';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {
  data;
  date: any[] = []
  roles: any[] = []
  arrayRoles: any[] = []
  currentUserId
  private readonly notifier: NotifierService;
  constructor(private applicationService: ApplicationService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    notifierService: NotifierService,
    public auth: AuthService) { this.notifier = notifierService;}

  ngOnInit() {
    this.applicationService.getCurrentUser().subscribe(file =>  {
      console.log(file.json().isLogged);
      this.currentUserId = file.json().isLogged.id
    });
    this.applicationService.getUsers().subscribe(file => {
      this.data = file.json();
      this.data.forEach(element => {
        this.roles = []
        this.date[element.id] =  moment(element.date, "YYYYMMDD h:mm:ss").fromNow()
        element.roles.forEach(role => {
          this.roles.push(role.replace('ROLE_', ' '))
        });
        this.arrayRoles[element.id] = this.roles
      });
      console.log(this.data);
      
    });
  }
  blockUser(id) {
    const user ={
      is_active : false
    }
    this.applicationService.deactivateUser(user, id).subscribe(res => {
      this.notifier.notify("success", "User is deactivated")
      this.ngOnInit();
    }, (err) => {
      this.notifier.notify("error", err.json().error.message)
    })
  }
  activateUser(id) {
    const user ={
      is_active : true
    }
    this.applicationService.activateUser(user, id).subscribe(res => {
      this.notifier.notify("success", "User is activated")
      this.ngOnInit();
    }, (err) => {
      this.notifier.notify("error", err.json().error.message)
    })
  }
  showUser(id, username) {
    this.router.navigate([`admin/user-profil/${id}/${username}`], id);
  }
}
