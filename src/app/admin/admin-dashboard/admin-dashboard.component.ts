import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators, FormControl  } from '@angular/forms';
import { NotifierService } from 'angular-notifier';

import { ApplicationService } from '../../services/application.service';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  allProducts;
  productReq = []
  numbers;
  ready = false
  private readonly notifier: NotifierService;
  constructor(private applicationService: ApplicationService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    notifierService: NotifierService,
    public auth: AuthService) { this.notifier = notifierService;}

  ngOnInit() {
    this.productReq = []
    this.applicationService.getProducts().subscribe(file => {
      this.allProducts = file.json()
      this.allProducts.forEach(product => {
        if (product.is_active == false) {
          this.productReq.push(product)
        }
      });
    }, (err) => {}, () => {
      this.applicationService.getAdminDashboardNumbers().subscribe(file => {
        this.numbers = file.json()[0]
      }, (err) => {}, () => {
        this.ready = true
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "./assets/admin/js/lib/calendar-2/pignose.calendar.min.js";
        document.body.append(s)
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "./assets/admin/js/lib/calendar-2/pignose.init.js";
        document.body.append(s)
      })
    })
  }

  removeFromShop(id) {
    const product ={
      is_active : false
    }
    this.applicationService.removeProductFromShop(product, id).subscribe(res => {
      this.notifier.notify("success", "Product removed from shop")
      this.ngOnInit();
    }, (err) => {
      this.notifier.notify("error", err.json().message)
    })
  }
  addToShop(id) {
    const product ={
      is_active : true
    }
    this.applicationService.addProductToShop(product, id).subscribe(res => {
      this.notifier.notify("success", "Product added to shop")
      this.ngOnInit();
    }, (err) => {
      console.log(err.json());
      
      this.notifier.notify("error", err.json().message)
    })
  }
}
