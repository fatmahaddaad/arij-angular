import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUserProfilComponent } from './admin-user-profil.component';

describe('AdminUserProfilComponent', () => {
  let component: AdminUserProfilComponent;
  let fixture: ComponentFixture<AdminUserProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUserProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUserProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
