import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators, FormControl  } from '@angular/forms';

import { ApplicationService } from '../../services/application.service';
@Component({
  selector: 'app-admin-user-profil',
  templateUrl: './admin-user-profil.component.html',
  styleUrls: ['./admin-user-profil.component.css']
})
export class AdminUserProfilComponent implements OnInit {
  private readonly notifier: NotifierService;
  currentUserId
  data
  birthdate
  date : any[] = [];
  ProductDate : any[] = [];
  roles: any[] = []
  arrayRoles: any[] = []
  updateForm: FormGroup;
  public imagePath;
  imgURL: any;
  fileToUpload: File = null;
  public message: string;
  isProvider: boolean;
  constructor(private applicationService: ApplicationService,
    private route: ActivatedRoute,
    private router: Router,
    notifierService: NotifierService) {this.notifier = notifierService; }

  ngOnInit() {
    this.applicationService.getCurrentUser().subscribe(file =>  {
      console.log(file.json().isLogged);
      this.currentUserId = file.json().isLogged.id
    });
    let id = this.route.snapshot.paramMap.get('id');
    this.applicationService.getAdmiUserProfile(id).subscribe(file => {
      this.data = file.json()
      console.log(this.data);
      if (this.data.user.roles.includes('ROLE_PROVIDER')) {
        this.isProvider = true
      }
      this.roles = []
      this.date[this.data.user.id] =  moment(this.data.user.date_creation, "YYYYMMDD h:mm:ss").fromNow()
      this.data.products.forEach(product => {
        this.ProductDate[product.id] =  moment(product.date_creation, "YYYYMMDD h:mm:ss").fromNow()
      });
      this.data.user.roles.forEach(role => {
        this.roles.push(role.replace('ROLE_', ' '))
      });
      this.arrayRoles[this.data.user.id] = this.roles
      this.birthdate = moment(this.data.user.birthdate, "YYYYMMDDTh:mm:ss").format('DD-MM-YYYY')
      
      /**
       * Form control to get user's date in update form
       */
      this.updateForm = new FormGroup({
        firstname: new FormControl(this.data.user.firstname),
        lastname: new FormControl(this.data.user.lastname),
        username: new FormControl({value: this.data.user.username, disabled: true}, [Validators.required]),
        email: new FormControl({value: this.data.user.email, disabled: true}, [Validators.required, Validators.email]),
        address: new FormControl(this.data.user.address),
        company: new FormControl(this.data.user.company),
        mat_number: new FormControl(this.data.user.mat_number),
        iban: new FormControl(this.data.user.iban),
        birthdate: new FormControl(moment(this.data.user.birthdate, "YYYYMMDDTh:mm:ss").format('YYYY-MM-DD')),
      });

    })
  }

  handleFileInput(files) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    this.fileToUpload = files[0]; 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }
  cancel_upload() {
    this.imgURL = "";
  }
  onUpdatePic() {
    let id = this.route.snapshot.paramMap.get('id');
    this.applicationService.AdminSetUserProfilePicture(this.fileToUpload, id).subscribe(res =>
      {
      this.notifier.notify( 'success', 'Profile picture uploaded!' );
      }, (err) => {
        console.log(err);
        this.notifier.notify( 'error', 'An error occurred while uploading profile picture' );
      });
  }

  blockUser(id) {
    const user ={
      is_active : false
    }
    this.applicationService.deactivateUser(user, id).subscribe(res => {
      this.notifier.notify("success", "User is deactivated")
      this.ngOnInit();
    }, (err) => {
      this.notifier.notify("error", err.json().error.message)
    })
  }
  activateUser(id) {
    const user ={
      is_active : true
    }
    this.applicationService.activateUser(user, id).subscribe(res => {
      this.notifier.notify("success", "User is activated")
      this.ngOnInit();
    }, (err) => {
      this.notifier.notify("error", err.json().error.message)
    })
  }

  onUpdate(id) {
    const user = this.updateForm.value;
    if (user.birthdate == 'Invalid date') {
      user.birthdate = null
    }
    console.log(user);
    this.applicationService.adminEditUserProfile(user, id).subscribe(res =>
    {
      this.notifier.notify( 'success', 'Profile Updated Successfully' );
      this.ngOnInit();
    }, (err) => {
      console.log(err);
      this.notifier.notify( 'error', 'error' );
    });
  }
}
