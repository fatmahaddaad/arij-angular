import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators, FormControl  } from '@angular/forms';

import { ApplicationService } from '../../services/application.service';
@Component({
  selector: 'app-admin-product',
  templateUrl: './admin-product.component.html',
  styleUrls: ['./admin-product.component.css']
})
export class AdminProductComponent implements OnInit {
  data: any;
  public imagePath;
  imgURL: any;
  fileToUpload: File = null;
  public message: string;
  date : any[] = [];
  updateForm: FormGroup;
  private readonly notifier: NotifierService;
  constructor(private applicationService: ApplicationService,
    private route: ActivatedRoute,
    private router: Router,
    notifierService: NotifierService) {this.notifier = notifierService; }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.applicationService.adminProductShow(id).subscribe(file => {
      this.data = file.json()
      this.date[this.data.id] =  moment(this.data.date_creation, "YYYYMMDD h:mm:ss").fromNow()
      console.log(this.data);
      
      /**
       * Form control to get product's data in update form
       */
      this.updateForm = new FormGroup({
        selling_price: new FormControl(this.data.selling_price, [Validators.required]),
        stock: new FormControl(this.data.stock, [Validators.required]),
      });
    });

  }

  handleFileInput(files) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    this.fileToUpload = files[0]; 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }
  cancel_upload() {
    this.imgURL = "";
  }

  removeFromShop(id) {
    const product ={
      is_active : false
    }
    this.applicationService.removeProductFromShop(product, id).subscribe(res => {
      this.notifier.notify("success", "Product removed from shop")
      this.ngOnInit();
    }, (err) => {
      this.notifier.notify("error", err.json().message)
    })
  }
  addToShop(id) {
    const product ={
      is_active : true
    }
    this.applicationService.addProductToShop(product, id).subscribe(res => {
      this.notifier.notify("success", "Product added to shop")
      this.ngOnInit();
    }, (err) => {
      console.log(err.json());
      
      this.notifier.notify("error", err.json().message)
    })
  }

  onUpdate(id) {
    const product = this.updateForm.value;
    this.applicationService.adminEditProduct(product, id).subscribe(res =>
      {
        this.notifier.notify( 'success', 'Product Updated Successfully' );
        this.ngOnInit();
      }, (err) => {
        console.log(err);
        this.notifier.notify( 'error', 'error' );
      });
  }
}
