import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {
  interface;
  constructor(private http: Http) { }
  Url = 'http://127.0.0.1:8000/';
  /**
   * Add token to headers
   * @param headers 
   */
  createAuthorizationHeader(headers: Headers) {
    headers.append('Authorization', 'Bearer '+localStorage.getItem('token')); 
  }
  /**
   * register new user
   * @param user 
   */
  createUser(user) {
    const formData: FormData = new FormData();
    formData.append('username', user.username);
    formData.append('email', user.email);
    formData.append('password', user.password);
    return this.http.post(this.Url+"register", formData);
  }
  /**
   * user login
   * @param user 
   */
  loginUser(user) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.post(this.Url+`login_check`, user, {headers});
  }
  /**
   * returns logged in user's ID, username and roles
   */
  getCurrentUser() {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(this.Url+`api`, {headers});
  }

  SupplierRegister(user) {
    const formData: FormData = new FormData();
    formData.append('username', user.username);
    formData.append('email', user.email);
    formData.append('password', user.password);
    formData.append('firstname', user.firstname);
    formData.append('lastname', user.lastname);
    formData.append('company', user.company);
    formData.append('address', user.address);
    formData.append('matnumber', user.matnumber);
    formData.append('iban', user.iban);
    return this.http.post(this.Url+"SupplierRegister", formData);
  }
  /**
   * get users list for admin part
   */
  getUsers() {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(this.Url+`users`, {headers});
  }
  /**
   * deactivate user by Admin
   * @param user 
   * @param id 
   */
  deactivateUser(user, id) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(this.Url+`deactivateUser/${id}`, user, {headers});
  }
  /**
   * activate user by admin
   * @param user 
   * @param id 
   */
  activateUser(user, id) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(this.Url+`activateUser/${id}`, user, {headers});
  }

  getAdmiUserProfile(id) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(this.Url+`adminUserProfile/${id}`, {headers});
  }

  AdminSetUserProfilePicture(fileToUpload: File, id) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    const formData: FormData = new FormData();
    formData.append('picture', fileToUpload, fileToUpload.name);
    return this.http.post(this.Url+`adminSetUserProfilePicture/${id}`, formData, {headers});
  }

  adminEditUserProfile(user, id) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(this.Url+`adminEditUserProfile/${id}`, user, {headers});
  }

  addProductToShop(product, id) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(this.Url+`addProductToShop/${id}`, product, {headers});
  }

  removeProductFromShop(product, id) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(this.Url+`removeProductFromShop/${id}`, product, {headers});
  }
  /**
   * get products list for admin part
   */
  getProducts() {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(this.Url+`products`, {headers});
  }

  getAdminDashboardNumbers() {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(this.Url+`adminDashboardNumbers`, {headers});
  }

  adminProductShow(id) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(this.Url+`adminProductShow/${id}`, {headers});
  }

  adminEditProduct(product, id) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(this.Url+`adminEditProduct/${id}`, product, {headers});
  }

  productsPublicShow() {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(this.Url+`productsPublicShow`, {headers});
  }

  categoriesPublicShow() {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(this.Url+`categoriesPublicShow`, {headers});
  }
  categoryPublicShow(id) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(this.Url+`categoryPublicShow/${id}`, {headers});
  }

  SupplierProductRequest(fileToUpload: File, product) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    const formData: FormData = new FormData();
    formData.append('label', product.label);
    formData.append('type', product.type);
    formData.append('sizes', product.sizes);
    formData.append('colors', product.colors);
    formData.append('price', product.price);
    formData.append('delivery_type', product.delivery_type);
    formData.append('delivery_time', product.delivery_time);
    formData.append('delivery_price', product.delivery_price);
    formData.append('description', product.description);
    formData.append('stock', product.stock);
    formData.append('picture', fileToUpload, fileToUpload.name);
    return this.http.post(this.Url+`SupplierProductRequest`, formData, {headers});
  }

  public loadScript(url) {        
    var isFound = false;
    var scripts = document.getElementsByTagName("script")
    for (var i = 0; i < scripts.length; ++i) {
        if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("loader")) {
            isFound = true;
        }
    }
  
    if (!isFound) {
        var dynamicScripts = [url];
  
        for (var i = 0; i < dynamicScripts .length; i++) {
            let node = document.createElement('script');
            node.src = dynamicScripts [i];
            node.type = 'text/javascript';
            node.async = false;
            node.charset = 'utf-8';
            document.getElementsByTagName('head')[0].appendChild(node);
        }
  
    }
  }
}
