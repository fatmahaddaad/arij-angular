import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(public auth: AuthService, public router: Router) { }

  canActivate(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    const allowedRoles = next.data.allowedRoles; // get allowedRoles from activated route
    if (!this.auth.isAuthenticated(allowedRoles)) { // check whether current user allowed to access route
      this.router.navigate(['/forbidden']);
      return false;
    }
    return true;
  }
}
