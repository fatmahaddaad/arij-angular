import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators, FormControl  } from '@angular/forms';
import { NotifierService } from 'angular-notifier';

import { ApplicationService } from '../services/application.service';
import { CartService } from '../cart/service/cart.service';
@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.css']
})
export class CategorieComponent implements OnInit {
  categoryy;
  data;
  creation_date;
  newProducts : any[] = []
  product;
  quantite=1;
  private readonly notifier: NotifierService;
  loadAPI: Promise<any>;
  constructor(private applicationService: ApplicationService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    notifierService: NotifierService,
    public cartservice:CartService,) { 
    this.loadAPI = new Promise((resolve) => {
      applicationService.loadScript("../../assets/user/js/main.js");
      resolve(true);
    })
  }
  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.applicationService.productsPublicShow().subscribe(file => {
      this.data = file.json();
      this.data.forEach(item => {
        this.creation_date = moment(item.date_creation);
        if (item.category.id == id) {
          this.newProducts.push(item);
        }
      });
      console.log(this.newProducts);
    })
    this.applicationService.categoryPublicShow(id).subscribe(file => {
      this.categoryy = file.json();
      console.log(this.categoryy);
    })
  }
  AddToCart(item){
    this.product = item;
    this.product.size = item.sizes[0];
    this.product.color = item.colors[0];
    console.log(item);
   let cart={'id':null, 'product':item, 'quantite':this.quantite, "color":item.colors[0], "size":item.sizes[0]};
    this.cartservice.setNbrArticleInCart(cart);
  }
}
