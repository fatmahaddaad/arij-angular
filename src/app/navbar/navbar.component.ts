import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { ApplicationService } from '../services/application.service';
import { AuthService } from '../services/auth.service';
import { CartService } from '../cart/service/cart.service'
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  logged
  isAdmin: boolean
  isProvider: boolean
  isClient: boolean
  constructor(private applicationService: ApplicationService,
    private route: ActivatedRoute,
    private router: Router,
    public auth: AuthService,
    public cartService: CartService) { }

  ngOnInit() {
    if (this.auth.isAuthenticated([])) {
      this.applicationService.getCurrentUser().subscribe(file => {
        this.logged = file.json().isLogged
        if (this.logged.roles.includes('ROLE_ADMIN')) {
          this.isAdmin = true
        }
        if (this.logged.roles.includes('ROLE_PROVIDER')) {
          this.isProvider = true
        }
        if (this.logged.roles.includes('ROLE_USER')) {
          this.isClient = true
        }
        console.log(this.auth.isAuthenticated([]));
      })
    }
  }

  onLogout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
    this.router.navigate([`/login`]);
  }
}
