import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { NotifierModule } from 'angular-notifier';
import { JwtModule } from '@auth0/angular-jwt';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgDynamicBreadcrumbModule} from "ng-dynamic-breadcrumb";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterTopComponent } from './footer-top/footer-top.component';
import { FooterComponent } from './footer/footer.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { CategorieComponent } from './categorie/categorie.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { SupplierRequestComponent } from './supplier-request/supplier-request.component';
import { AdminNavbarComponent } from './admin/admin-navbar/admin-navbar.component';
import { AdminLeftSidebarComponent } from './admin/admin-left-sidebar/admin-left-sidebar.component';
import { AdminUsersComponent } from './admin/admin-users/admin-users.component';
import { AdminUserProfilComponent } from './admin/admin-user-profil/admin-user-profil.component';
import { AdminProductsComponent } from './admin/admin-products/admin-products.component';
import { AdminProductComponent } from './admin/admin-product/admin-product.component';
import { SupplierRequestProductComponent } from './supplier-request-product/supplier-request-product.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';


export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    AdminComponent,
    AdminLoginComponent,
    ForbiddenComponent,
    NavbarComponent,
    FooterTopComponent,
    FooterComponent,
    BreadcrumbComponent,
    ProductsComponent,
    ProductComponent,
    CategorieComponent,
    CartComponent,
    CheckoutComponent,
    SupplierRequestComponent,
    AdminNavbarComponent,
    AdminLeftSidebarComponent,
    AdminUsersComponent,
    AdminUserProfilComponent,
    AdminProductsComponent,
    AdminProductComponent,
    SupplierRequestProductComponent,
    AdminDashboardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    NotifierModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:4200'],
        blacklistedRoutes: []
      }
    }),
    NgxPaginationModule,
    ReactiveFormsModule,
    NgDynamicBreadcrumbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
