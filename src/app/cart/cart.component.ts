import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { ApplicationService } from '../services/application.service';
import { CartService } from './service/cart.service';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  rowscarts;
  totalCart = 0;
  rowscartsLocal: LocalDataSource;
  constructor(public cartserv: CartService, 
    private router: Router,
    public auth: AuthService,
    private applicationService:ApplicationService) { 
    if (cartserv.verifLocalStorageCart()) { 
      let storage = JSON.parse(localStorage.getItem('carts'))
      if (storage.length > 0) {
        this.rowscarts = storage;
        cartserv.carts = this.rowscarts
      }
      else
        this.rowscarts = cartserv.carts;
    }else{
      this.rowscarts =[]
      cartserv.carts = this.rowscarts
    }
    this.calculTotalCart(this.rowscarts);
    this.rowscartsLocal = new LocalDataSource();
    this.rowscartsLocal.load(cartserv.carts);
  }

  ngOnInit() {
    console.log(this.rowscarts);
    
  }
  async onChange(event, row) {
    console.log(event.path[3].cells.item(4).textContent)
    //let total:number=+event.path[3].lastChild.textContent;
    let quantite: number = +event.target.value
    let total: number = row.product.price * quantite;
    event.path[3].cells.item(4).textContent = total + " DT";
    console.log(row)
    let newrow = Object.assign({}, row)
    newrow.quantite = quantite;
    this.rowscartsLocal.update(row, newrow);
    this.rowscarts = await this.rowscartsLocal.getAll();
    this.cartserv.carts = this.rowscarts;
    this.cartserv.nbrArticleInCart = this.rowscarts.length;
    localStorage.setItem('carts', JSON.stringify(this.rowscarts))
    this.calculTotalCart(this.rowscarts)
  }
  async delete(event, cart) {
    console.log("delete", cart);
    this.rowscartsLocal.remove(cart);
    this.rowscarts = await this.rowscartsLocal.getAll();
    this.cartserv.carts = this.rowscarts;
    this.cartserv.nbrArticleInCart = this.rowscarts.length;
    localStorage.setItem('carts', JSON.stringify(this.rowscarts))
    this.calculTotalCart(this.rowscarts)
  }
  calculTotalCart(row) {
    this.totalCart = row.map(el => el.product.price * el.quantite).reduce((acc, el) => acc + el, 0);
  }
  detail(rowcarts) {
    this.applicationService.interface="cart";
    this.router.navigate(['/product', rowcarts.product.id])
  }
  clearCart(){
    localStorage.setItem('carts','')
    this.rowscartsLocal.reset();
    this.rowscarts = [];
    this.cartserv.carts = [];
    this.cartserv.nbrArticleInCart = this.rowscarts.length;
    this.totalCart=0;
  }
  async getColorOrSize(event,colorOrSize,rowcart,type){
  console.log(event)
  let newrow = Object.assign({}, rowcart)
  if(type==="color")
  newrow.color=colorOrSize;
  else
  newrow.size=colorOrSize;
  this.rowscartsLocal.update(rowcart, newrow);
  this.rowscarts = await this.rowscartsLocal.getAll();
  this.cartserv.carts = this.rowscarts;
  localStorage.setItem('carts', JSON.stringify(this.rowscarts))
  }
}
