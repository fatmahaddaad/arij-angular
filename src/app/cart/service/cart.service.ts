import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  carts=[];
  nbrArticleInCart:number=0;
  constructor() {
    this.nbrArticleInCart = 0;
    console.log("My global variable value: " + this.nbrArticleInCart);
  }

  async setNbrArticleInCart(cart) {
    /*test this.carts with localStorage*/
    if(this.verifLocalStorageCart())
    this.carts=JSON.parse(localStorage.getItem('carts'));
  
    let exist=false;
    this.carts.forEach(element => {
      if(element.product.id == cart.product.id) {
        exist=true;
        return false
      }
    });
    if(exist===false) {
      this.carts.push(cart);
      localStorage.setItem('carts',JSON.stringify(this.carts));
      this.nbrArticleInCart = this.carts.length;
      console.log("Cart",this.carts)
    } else {
      alert("Already in cart")
    }
  }
  
  getNbrArticleInCart(){
    return this.nbrArticleInCart;
  }
  getCart() {
    return this.carts;
  }
  verifLocalStorageCart() {
    if(localStorage.getItem('carts')) {
      if(localStorage.getItem('carts')==='')
      return false;
      else
      return true;
    }
     else 
     return false
  }
}
