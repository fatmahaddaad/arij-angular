import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { CartComponent } from './cart/cart.component';
import { CategorieComponent } from './categorie/categorie.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';
import { SupplierRequestComponent } from './supplier-request/supplier-request.component';
import { AdminUsersComponent } from './admin/admin-users/admin-users.component';
import { AdminUserProfilComponent } from './admin/admin-user-profil/admin-user-profil.component';
import { AdminProductsComponent } from './admin/admin-products/admin-products.component';
import { AdminProductComponent } from './admin/admin-product/admin-product.component';
import { SupplierRequestProductComponent } from './supplier-request-product/supplier-request-product.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';

const routes: Routes = [
  { path: '' , component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'supplier-request', component: SupplierRequestComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'admin-login' , component: AdminLoginComponent},
  { path: 'product-request', component: SupplierRequestProductComponent, canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_PROVIDER']}},
  { path: 'forbidden', component: ForbiddenComponent, data: {
      title: 'login',
      breadcrumb: [
        {
          label: 'Home',
          url: '/'
        },
        {
          label: 'forbidden',
          url: ''
        }
      ] 
    },
  },
  { path: 'cart', component: CartComponent, data: {
      title: 'cart',
      breadcrumb: [
        {
          label: 'Home',
          url: '/'
        },
        {
          label: 'cart',
          url: ''
        }
      ] 
    },
  },
  { path: 'category/:id', component: CategorieComponent, data: {
      title: 'category',
      breadcrumb: [
        {
          label: 'Home',
          url: '/'
        },
        {
          label: 'category',
          url: ''
        }
      ] 
    },
  },
  { path: 'checkout', component: CheckoutComponent, data: {
      title: 'checkout',
      breadcrumb: [
        {
          label: 'Home',
          url: '/'
        },
        {
          label: 'checkout',
          url: ''
        }
      ] 
    },
  },
  { path: 'product/:id', component: ProductComponent, data: {
      title: 'product',
      breadcrumb: [
        {
          label: 'Home',
          url: '/'
        },
        {
          label: 'product',
          url: ''
        }
      ] 
    },
  },
  { path: 'shop', component: ProductsComponent, data: {
      title: 'shop',
      breadcrumb: [
        {
          label: 'Home',
          url: '/'
        },
        {
          label: 'shop',
          url: ''
        }
      ] 
    },
  },
  { path: 'admin', component: AdminComponent, canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_ADMIN']},
    children: [
      {path: '', component: AdminDashboardComponent},
      {path: 'users', component: AdminUsersComponent},
      {path: 'user-profil/:id/:username', component: AdminUserProfilComponent},
      {path: 'products', component: AdminProductsComponent},
      {path: 'edit-product/:id', component: AdminProductComponent},
    ] 
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
