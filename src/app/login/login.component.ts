import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NotifierService } from 'angular-notifier';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Http } from '@angular/http';

import { ApplicationService } from '../services/application.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private readonly notifier: NotifierService;
  username: string;
  password: string;
  constructor(private applicationService: ApplicationService,
    notifierService: NotifierService,
    private route: ActivatedRoute,
    private router: Router,
    private http: Http) {
      this.notifier = notifierService;
     }

  ngOnInit() {
  }

  onLogin(){
    const user ={
      username : this.username,
      password : this.password
    }
    if (user.username && !user.password) {
      this.notifier.notify( 'error', 'password required' );
    } else if (!user.username && user.password) {
      this.notifier.notify( 'error', 'username required' );
    } else if (user.password && user.username) {
      this.applicationService.loginUser(user).subscribe(res =>
      {
        localStorage.setItem('token', JSON.parse(res.text()).token);
        this.notifier.notify( 'success', 'User Authentificated Successfully' );
        this.router.navigate([`/`]);
      }, (err)=> {
        this.notifier.notify( 'error', 'An error occurred while logging in, please check your credentials or contact the admin');
      });
    } else {
      this.notifier.notify( 'error', 'username and password required' );
    }
  }
}
